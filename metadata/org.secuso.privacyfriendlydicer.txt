Categories:Games
License:GPLv3
Web Site:https://secuso.informatik.tu-darmstadt.de/en/secuso-home
Source Code:https://github.com/SecUSo/privacy-friendly-dicer
Issue Tracker:https://github.com/SecUSo/privacy-friendly-dicer/issues

Auto Name:Dicer
Summary:Roll dices
Description:
Simple dicing application which can be used to roll up to ten six-sided dice.
Dicing can be done by pressing a button or shaking the smart phone. It belongs
to the group of Privacy Friendly Apps from the research group SecUSo (Security,
Usability and Society) by the Technische Universität Darmstadt, Germany.
.

Repo Type:git
Repo:https://github.com/SecUSo/privacy-friendly-dicer

Build:1.0,1
    commit=565ba3da15feb1341789c4cbc38c0797c6b6b9d5
    subdir=app
    gradle=yes

Maintainer Notes:
Package id changed and no new tag, so UCM:Tags checks the wrong id. Reset
after new version is done.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1
