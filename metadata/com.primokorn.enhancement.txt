Categories:Theming
License:GPLv3
Web Site:https://gitlab.com/Primokorn/EnhancementTheme
Source Code:https://gitlab.com/Primokorn/EnhancementTheme/tree/HEAD
Issue Tracker:https://gitlab.com/Primokorn/EnhancementTheme/issues

Auto Name:Enhancement
Summary:White theme for CM13
Description:
Enhancement is a theme for custom ROMs having the CM13 theme engine. The goal is
to provide a stock UI with enhanced icons and colors for your device, keeping it
light and clean.
.

Repo Type:git
Repo:https://gitlab.com/Primokorn/EnhancementTheme.git

Build:3.0,3
    commit=f212116518ca91f71cea45103410409823bb27b3
    subdir=theme
    gradle=yes

Build:4.0,4
    commit=f75fcd711ec85c5ec8095d6920376f0cbef83cb5
    subdir=theme
    gradle=yes

Build:5.0,5
    commit=74b4919ee6b725fd98e9da4f3a65e657c157e583
    subdir=theme
    gradle=yes

Build:6.0,6
    commit=0812e4e9af002156cdeb3545471dfdf2f57179af
    subdir=theme
    gradle=yes

Build:9.0,9
    commit=2a42349af0536e31082c7892ad6cfbf92c1bfe46
    subdir=theme
    gradle=yes

Build:10.0,10
    commit=68f7136481dd7b465ebcb3f9015b1b4a793f4747
    subdir=theme
    gradle=yes

Build:11.0,11
    commit=4f9a85c21d8abb3aa8a688c1878d781e9116b9d9
    subdir=theme
    gradle=yes

Build:12.0,12
    commit=12.0
    subdir=theme
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:12.0
Current Version Code:12
